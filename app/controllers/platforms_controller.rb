class PlatformsController < ApplicationController
  before_action :validate_id, only: [:by_date, :by_range_date]
  before_action :validate_date, only: [:by_date]
  before_action :validate_date_range, only: [:by_range_date]

  def by_date
    render json: Platform.by_date(params[:id], @date)
  end

  def by_range_date
    render json: Platform.by_range_date(params[:id], @start_date, @end_date)
  end

  private

  def validate_id
    @msgs ||= {}

    unless Platform.id_existed?(params[:id])
      @msgs[:id] = {msg: "未找到对应的平台"}
    end

    render json: @msgs, status: 400 if @msgs.present?
  end

  def validate_date
    @msgs ||= {}

    unless params[:date]
      @msgs[:date] = {msg: "不需要包含date参数"}
    else
      parse_date
    end

    render json: @msgs, status: 400 if @msgs.present?
  end

  def validate_date_range
    @msgs ||= {}

    unless params[:start_date]
      @msgs[:start_date] = {msg: "需要包含start_date参数"}
    else
      parse_date
    end

    unless params[:end_date]
      @msgs[:end_date] = {msg: "需要包含end_date参数"}
    else
      parse_date
    end

    render json: @msgs, status: 400 if @msgs.present?
  end

  def parse_date
    begin
      @date = DateTime.parse(params[:date]) if params[:date]
      @start_date = DateTime.parse(params[:start_date]) if params[:start_date]
      @end_date = DateTime.parse(params[:end_date]) if params[:end_date]
    rescue => e
      @msgs[:date] = {msg: "输入的日期格式不正确"}
    end
  end
end
