class Platform

  UIDS = ["3", "6"] # 云商(uid=3) 表示云商平台数据, 益农(uid=6) 表示益农平台数据

  class << self
    def id_existed?(uid)
      UIDS.include?(uid)
    end

    def by_date(uid, date)
      whole_day_of_date = date.beginning_of_day..date.end_of_day
      SEQ_BI_DB[:adstat].where(vdate: whole_day_of_date, uid: uid).order(:vdate).last || {}
    end

    def by_range_date(uid, start_date, end_date)
      start_date_time = start_date.beginning_of_day
      end_date_time = end_date.end_of_day
      all_days_range = start_date_time..end_date_time
      SEQ_BI_DB[:adstat].where(vdate: all_days_range, uid: uid).order(:vdate)
    end
  end
end
